# google-appscript-base-ts

## TypeScript Ready GAS

This base package can be cloned to have a GAS environement with TypeScript ready to use.

To compile, use the tsc command
To upload the script, either add an existing script ID to script/.clasp.json, or create a script with clasp create.

Use clasp push within /script to upload.
